import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url'
import {
   View,
   Text,
   TextInput,
   TouchableOpacity,
   StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import Axios from 'axios';

const AddData = ({
   navigation,
   route
}) => {

    var dataMobil = route.params;
    
    const [namaMobil, setNamaMobil] = useState('');
    const [totalKM, setTotalKM] = useState('');
    const [hargaMobil, setHargaMobil] = useState('');
    const [gambar, setGambar] = useState('');

    const postData = async () => {
        const body = [
            {
                title: namaMobil,
                harga: hargaMobil,
                totalKM: totalKM,
                unitImage: gambar,
                // unitImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU"
            }
        ]

        const options = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": TOKEN
                }     
        }
        Axios.post(`${BASE_URL}mobil`, body, options )
        .then(response => {
            console.log('berhasil', response);
            if(response.status === 200 || response.status === 201){
                alert('Data Berhasil ditambah')
                navigation.goBack();
            }
        })
        .catch((error) => {
            console.log('gagal tambah data', error)
        })
       
    }

    const editData = async () => {
        const body = [
            {
                _uuid: dataMobil._uuid,
                title: namaMobil,
                harga: hargaMobil,
                totalKM: totalKM,
                unitImage: gambar,
                // unitImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU"
            }
        ]
        const options = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": TOKEN
                }}

        Axios.put(`${BASE_URL}mobil`, body, options)   
        .then(response => {
            console.log('berhasil', response);
            if(response.status === 200 || response.status === 201){
                alert('Data Berhasil diubah')
                navigation.goBack();
            }
        })
        .catch((error) => {
            console.log('gagal ubah data', error)
        })


    }
 

    const deleteData = async () => {
        const body = [
         {
           _uuid: dataMobil._uuid,
         }
        ]
        const options = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": TOKEN
                }}
                
            Axios.delete(`${BASE_URL}mobil`, {data: body, ...options})
            .then((response) => {
                console.log('berhasil', response);
                if(response.status === 200 || response.status === 201){
                    alert('Data Berhasil dihapus')
                    navigation.goBack();
                }
                
            })
            .catch((error) => {
                console.log('gagal dihapus', error)
            })
      }

      
      useEffect(() => {
        if(route.params) {
            const data = route.params;
            setNamaMobil(data.title);
            setTotalKM(data.totalKM);
            setHargaMobil(data.harga);
            setGambar(data.unitImage);
        }
     },[])

     const inputValid = value =>{
        if(!namaMobil){
            return alert('Nama mobil tidak boleh kosong bang!');
        }
        if(!totalKM){
            return alert('Total KM harus di isi!');
        }
        if(!hargaMobil) {
            
            return alert('Masa Gratis bang!');
        }else if(hargaMobil<=10000000){
            return alert('Minimal harga mobil 10 Juta!');
        }

        switch (value) {
            case 'post':
                postData();
                break;
            
            case 'edit':
                editData();
                break;
    
                default:
                    break;
            
         }
     }
     
     


 

   return(
       <View style={{flex: 1, backgroundColor: '#fff'}}>
           <View style={{
               width: '100%',
               flexDirection: 'row',
               alignItems: 'center'
           }}>
               <TouchableOpacity
                   onPress={() => navigation.goBack()}
                   style={{width: '10%', justifyContent: 'center', alignItems: 'center', paddingVertical: 10}}>
                   <Icon
                       name='arrowleft'
                       size={20}
                       color="#000"
                   />
               </TouchableOpacity>
               <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>{dataMobil ? "Ubah Data":"Tambah Data"}</Text>
           </View>
           <View style={{
               width: '100%',
               padding: 15
           }}>
               <View>
                   <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>Nama Mobil</Text>
                   <TextInput
                       placeholder='Masukkan Nama Mobil'
                       style={styles.txtInput}
                       value={namaMobil}
                       onChangeText={(text) => setNamaMobil(text)}
                   />
               </View>
               <View style={{marginTop: 20}}>
                   <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>Total Kilometer</Text>
                   <TextInput
                       placeholder='contoh: 100 KM'
                       style={styles.txtInput}
                       value={totalKM}
                       onChangeText={(text) => setTotalKM(text)}
                   />
               </View>
               <View style={{marginTop: 20}}>
                   <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>Harga Mobil</Text>
                   <TextInput
                       placeholder='Masukkan Harga Mobil'
                       style={styles.txtInput}
                       value={hargaMobil}
                       onChangeText={(text) => setHargaMobil(text)}
                       keyboardType="number-pad"
                   />
               </View>

               <View style={{marginTop: 20}}>
                   <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>Gambar</Text>
                   <TextInput
                       placeholder='link gambar'
                       style={styles.txtInput}
                       value={gambar}
                       onChangeText={(text) => setGambar(text)}
                   />
               </View>
               <TouchableOpacity
                   style={styles.btnAdd}
                   onPress={() => (dataMobil ? inputValid('edit') : inputValid('post'))}
               >
                   <Text style={{color: '#fff', fontWeight: '600'}}>{dataMobil? "Ubah Data" : "Tambah Data"}</Text>
               </TouchableOpacity>
               
               {
                dataMobil ?
               
               <TouchableOpacity
               style={[styles.btnAdd, {backgroundColor: 'red'}]}
               onPress={() => deleteData()}
               >
                <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
                </TouchableOpacity>

                : null
            }

           </View>
       </View>
   )
}

const styles = StyleSheet.create({
   btnAdd: {
       marginTop: 20,
       width: '100%',
       paddingVertical: 10,
       borderRadius: 6,
       backgroundColor: '#689f38',
       justifyContent: 'center',
       alignItems: 'center'
   },
   txtInput: {
       marginTop: 10,
       width: '100%',
       borderRadius: 6,
       paddingHorizontal: 10,
       borderColor: '#dedede',
       borderWidth: 1
   }
})

export default AddData;
